var amqp = require('amqplib/callback_api');

module.exports = producer;

function producer(data, options, done) {
    var q_name = options.q_name || 'test';
    var ex = options.ex || 'main';
    var key = options.key || 'test';
    var ex_type = options.ex_type || 'direct';
    var message = JSON.stringify(data) || 'empty message';

    console.log(' [qp] options: %s', JSON.stringify(options));

    amqp.connect('amqp://localhost', function(err, conn) {
        if (err != null) {
            console.error('Connection error: ' + err);
        }

        conn.createChannel(function(err, ch) {
            
            ch.on('close', function (err) {
                if (err) { 
                    console.error('Producer Channel Close Error: ' + err);
                }
                conn.close(function (err) {
                    if (err) {
                        console.error('Producer Connection Close Error: ' + err);
                    }
                    done();
                    console.log('Producer connection closed succesfully');
                });
            });
            process.on('exit', function () {
                ch.close();
            });

            ch.assertExchange(ex, ex_type, {durable: false});
            ch.bindQueue(q_name, ex, key);
            ch.publish(ex, key, new Buffer(message));
            ch.close();     
        });
    });
};