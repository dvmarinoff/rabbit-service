var amqp = require('amqplib/callback_api');

module.exports = consumer;

function consumer(work, options, cluster) {
    var q_name = options.q_name || 'test';
    var ex = options.ex || 'main';
    var key = options.key || 'test';
    var ex_type = options.ex_type || 'direct';

    // console.log(' [qp] %s, options: %s', cluster.worker.process.pid, JSON.stringify(options));

	amqp.connect('amqp://localhost', function(err, conn) {

	    if (err) {
	        console.error('Connection error: ' + err);
	    }

	    conn.createChannel(function(err, ch) {
		    if (err) {
		        console.error('Channel error: ' + err);
		        conn.close();
		    }
		    ch.on('close', function (err) {
	    		if (err) {
	    			console.error('Channel close error: ' + err);
	    		}
	        	conn.close(function(err) {
	        		if (err) {
	        			console.error('Connection close error: ' + err);
	        		}
		        	// cluster.worker.kill('SIGTERM');
	        	});
		    });
		    
	        ch.assertExchange(ex, 'direct', {durable: false});

	        ch.assertQueue(q_name, {durable: false}, function(err, q) {
			    if (err) {
			        console.error('Queue error: ' + err);
		        	ch.close();
			    }

		        ch.bindQueue(q_name, ex, key);
		        ch.prefetch(1);

				ch.consume(q_name, function(msg_consume) {
					var job = JSON.parse(msg_consume.content);

					work(job, cluster, function (result) {
						ch.ack(msg_consume);
						
						// console.log(" [w] %s %s", key.toUpperCase(), cluster.worker.process.pid);

						return result;
					});

				}, {noAck: false});    
	        });
		});
	});
}