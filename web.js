var express = require('express');
var bodyParser = require('body-parser');
var CronJob = require('cron').CronJob;
var config = require('./config/config');
var CaptureJob = require('./models/capture_job');
var Job = require('./models/job_model');
var User = require('./models/user_model');
var produce = require('./amqp/producer'); 
var db = require('./config/db');
var ObjectId = require('mongoose').Types.ObjectId;

var app = express();

db.connect(function () {
    console.log('connected to mongo');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// express.static('./storage/');

var port = config.port;

var router = express.Router();

router.get('/', function(req, res) {
    res.json({ message: 'phantomjs service api' });   
});

router.route('/jobs')
    .post(function (req, res) {
        
        console.log('jobCreateOnce: %s', JSON.stringify(req.body));
        var capture_job = new CaptureJob({"user":{"_id":"56f547f4fbcbc934615904cf","provider":"local","username":"user_abv","__v":1,"created":"2016-03-25T14:15:16.069Z","roles":["user","admin"],"email":"dvmarinov@abv.bg"},"_id":"570e4832ae53c0e84f7d3284","results":[],"email":"dvmarinov@abv.bg","preview":"","active":true,"service":0,"change":1,"cron":"0 7 * * *","url":"http://kipo.bg","created":"2016-04-13T13:22:58.183Z"});
        
        capture_job.isPreview = true;

        produce(capture_job, {q_name: 'test', ex: 'main', key: 'capture'}, function (result) {
            console.log(' [express] new Render added: ' + JSON.stringify(capture_job) + ' result: ' + JSON.stringify(result));
        });        
        
        if(capture_job.isPreview) {
             res.status(200).send('OK');
        } else {
             res.status(200).send('OK');
        }
});

router.route('/jobs/:jobId')
    .put(jobUpdate);

router.route('/schedule')
    // .post(jobSchedule)
    .post(function (req, res) {
        var j = new CaptureJob(req.body);
        var cron_config = {

        };
        // console.log('Job check in scheduler: ' + JSON.stringify(j));

        // var sj = new CronJob(j.cron, function () {
        //     var self = this;
            var id = new ObjectId(j._id);

            Job.findById(id).populate('user').exec(function(err, job) {
                if (err) {
                    console.error('Capture Job query error: %s', err);
                    return;
                }
                if(!job) {
                    console.error('Job not found! id: %s', id);
                    res.status(404).send('Not Found');
                } 
                if(!err && job) {

                    res.status(200).send('OK');
                    console.log('Job check in scheduler: ' + id + ' ' + JSON.stringify(job));
                }

            });



            //     console.log('Job check in scheduler: ' + JSON.stringify(job));
            // });

        // }, function () {
        //     sj.stop();
        // }, true);
        
        
        console.log(' [express] new Job scheduled: ' + JSON.stringify(req.body));
    })
    .delete(jobDelete);

// NOTE: All urls are prefixed with '/api' so '/jobs' is found at '/api/jobs' 
app.use('/api', router);

app.listen(port, function() {
    console.log('phantomjs service listening on port ' + port);
});

function jobDelete (req, res) {

}

function jobUpdate (req, res) {

}