#!/bin/bash

tab=" --tab-with-profile=mac"
options=(--tab --title=Terminal)

cmds=("node web.js" "node workers_capture.js" "node workers_compare.js")
titles=("HTTP" "CAPTURE" "COMPARE")

for i in "${!cmds[@]}"; do
    options+=( $tab --title="${titles[$i]}" -e "bash -c \"${cmds[$i]} ; bash\"" )          
done

gnome-terminal "${options[@]}"

# open in default browser rabbitmq management plugin web ui
# NOTE: to enable plugin run $ rabbitmq-plugins enable rabbitmq_management
if which xdg-open > /dev/null
then
    xdg-open http://localhost:15672/
elif which gnome-open > /dev/null
then
    gnome-open http://localhost:15672/
fi

exit 0
