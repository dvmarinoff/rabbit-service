module.exports = function(app) {
	// 404s
	app.use(function(req, res, next) {
		res.status(404);

		if (req.accepts(html)) {
			return res.send('<h2>Page Not Found<h2>');
		}
		if(req.accepts(json)) {
			return res.json({error: 'Not found'});
		}
		res.type('txt');
		res.send('Couldn\'t find that resourse');
	});
	// 500
	app.use(function(err, req, res, next) {
		console.error('server error at %s\n %s', req.url, err);
		res.send(500, 'Server error');
	});
};