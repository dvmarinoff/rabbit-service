var Url = require('url');
var driver = require('node-phantom-simple');
var Compare_job = require('../models/compare_job');
var produce = require('../amqp/producer');

var DEFAULT_VIEWPORT_SIZE = {};
var DEFAULT_CLIP_RECT = {};

module.exports = capture;

function capture (options, cluster, done) {
 	var viewportSize = DEFAULT_VIEWPORT_SIZE;
	var created = options.created; 
	var check_time = Date.now() / 1000 | 0;
	var user_dir = options.user._id || '';
	var job_dir = options._id || '';
	var isPreview = options.isPreview || false;
	var result_path = '';
	
	result_path = './storage/' + user_dir + '/' + job_dir + '/preview.png';
	// if(true) {
	// } else {
	// 	result_path = './storage/' + user_dir + '/' + job_dir + '/test_' + check_time +'.png';
	// }
	
	var optionsRender = {};

	driver.create({ path: require('phantomjs').path }, function (err, browser) {
 
	    return browser.createPage(function (err, page) {

			page.set('viewportSize', viewportSize, function () {
		    
			    page.open(options.url, function (err, status) {

					page.set('clipRect', options.clipRect, function () {

						page.render(result_path, optionsRender, function () {	        
				        	console.log(' [ph] render pid: ' + cluster.worker.process.pid + ' status: ' +  status + ' url: ' + options.url);

								if(isPreview) {
									console.log(' [ph] %s exit browser', cluster.worker.process.pid);

									browser.exit();
										
									return done({preview_path: result_path});
									
									// produce({preview_path: result_path}, {q_name: 'preview', ex: 'main', key: 'preview'}, function() {

									// 	console.log(' [ph] %s exit browser', cluster.worker.process.pid);
									// 	browser.exit();
										
									// 	return done({preview_path: result_path});
									// });
								} else {
									var compare_job = new Compare_job({path_new: result_path, job_id: options._id});
									
									produce(compare_job, {q_name: 'compare', ex: 'main', key: 'compare'}, function() {

										console.log(' [ph] %s exit browser', cluster.worker.process.pid);
										browser.exit();
										
										return done(compare_job);
									});
								}
				        });
					});
				});
			});
		});
	});
};