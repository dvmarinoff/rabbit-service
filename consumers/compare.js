var fs = require('fs');
var resemble = require('node-resemble-js');

module.exports = createDiff;

resemble.outputSettings({
	errorColor: {
		red: 255,
		green: 0,
		blue: 255
	},
	errorType: 'flat',
	transparency: 0.3
});

function createDiff(data, cluster, done) {
	data.path_base = './storage/48792810c0555e997c09q14i/56582810c0555e997c09fad9/test_base.png';
	if (data.path_base == undefined) {
		// TO DO: test only, comment it when done
		// return;
	}
	if (data.path_new == undefined) {
		console.log('Error path to new is undefined');	
	}

	// console.log('base: %s, new: %s ', data.path_base, data.path_new);

	var diff = resemble(data.path_base).compareTo(data.path_new).ignoreAntialiasing().onComplete(function(data) {
	    var time = Date.now().toString();
	    var result = './storage/48792810c0555e997c09q14i/56582810c0555e997c09fad9/diff_' + time + '.png';
	    
	    console.log(data);

	    data.getDiffImage().pack().pipe(fs.createWriteStream(result));
	    done();
	    // TO DO: write result to db collection results
	});
}