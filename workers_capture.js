var cluster = require('cluster');
var amqp = require('amqplib/callback_api');
var capture = require('./consumers/capture');
var consume = require('./amqp/consumer');
var workers_count = 4;

// TO DO: add PM dynamic cluster with gracefullReload event so
// workers can be added/removed according to needs without 
// restaring the service
if(cluster.isMaster) {

	for (var i = 0; i < workers_count; i++) {
		cluster.fork();
	}

	cluster.on('online', function(worker) {
		console.log('Worker ' + worker.process.pid + ' is online.');
	});
	cluster.on('exit', function(worker, code, signal) {
		console.log('Worker ' + worker.process.pid + ' exited with code ' + code);
		cluster.fork();
	});
} else {
	consume(capture, {q_name: 'test', ex: 'main', key: 'capture'}, cluster);
}