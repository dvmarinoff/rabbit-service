var cluster = require('cluster');
var amqp = require('amqplib/callback_api');
var consume = require('./amqp/consumer');
var createDiff = require('./consumers/compare');

var workers_count = 1;

if(cluster.isMaster) {

	for (var i = 0; i < workers_count; i++) {
		cluster.fork();
	}

	cluster.on('online', function(worker) {
		console.log('Worker ' + worker.process.pid + ' is online.');
	});
	cluster.on('exit', function(worker, code, signal) {
		console.log('Worker ' + worker.process.pid + ' exited with code ' + code);
		cluster.fork();
	});
} else {
	consume(createDiff, {q_name: 'compare', ex: 'main', key: 'compare'}, cluster);
}