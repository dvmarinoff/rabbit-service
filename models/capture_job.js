// TO DO: refactor to virtual props in the mongoose model
function CaptureJob (options) {
 	this._id = options._id || "56582810c0555e997c09fad9";
 	this.url = options.url || 'http://kipo.bg';
 	this.email = options.email || '';
 	this.preview = options.preview || '';
 	this.isPreview = options.isPreview || false;
 	this.active = options.active || true;
 	this.service = options.service || 0;
 	this.change = options.change || 2;
 	this.cron = options.cron || '0 0 1 * *';
 	this.clipRect = options.clipRect || {};
 	this.user = options.user;
}

module.exports = CaptureJob;