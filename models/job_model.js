var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var JobSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  url: {
    type: String,
    default: '',
    trim: true,
    required: 'Url cannot be blank'
  },
  cron: {
    type: String,
    default: '0 7 * * *',
    trim: true,
    required: 'Cron cannot be blank'
  },
  change: {
    type: Number,
    default: 1,
    trim: true,
    required: 'Change cannot be blank'
  },
  service: {
    type: Number,
    default: 0,
    trim: true,
    required: 'Service cannot be blank'
  },
  active: {
    type: Boolean,
    default: true,
    required: 'Active cannot be blank'
  },
  preview: {
    type: String,
    default: ''
  },
  email: {
    type: String,
    default: ''
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  results: {
    type: Array,
    default: []
  }
});

var Job = mongoose.model('Job', JobSchema);

module.exports = Job;