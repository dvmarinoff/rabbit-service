var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

module.exports = Result;

var ResultSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	path_new: {
		type: String,
		default: '',
	trim: true
	},
	path_base: {
		type: String,
		default: ,
	trim: true
	},
	path_diff: {
		type: String,
		default: '',
	trim: true
	},
	job: {
		type: Schema.ObjectId,
		ref: 'Job'
	}
});

var Result = mongoose.model('Result', ResultSchema);