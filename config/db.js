var mongoose = require('mongoose');
var config = require('./config');

function connect(done) {

    var db = mongoose.connect(config.meanDev, function (err) {
        if (err) {
            console.error('Mongo connection error: %s', err);
        } else {
            if (done) {
                return done(db);
            }
        }
    });
};

module.exports.connect = connect; 